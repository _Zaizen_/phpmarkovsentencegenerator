PHPMarkovSentenceGenerator
============

[![License: LGPL v3](https://img.shields.io/badge/License-LGPL_v3-blue.svg)](https://www.gnu.org/licenses/lgpl-3.0)


With this you can generate random sentences thanks to a markov chain.
It also stores the source texts and generated ones in a db.

A huge shoutout goes also to https://github.com/uhho/PHPMarkovChain for
providing a simple library to create a markov chain.

And of course, please checkout [Gabriele Trevisan](https://gitlab.com/gabrieletrevisan02) too, he's the main
author behind most of the PHP, I'm just the one who host everything
and collect datasets :D