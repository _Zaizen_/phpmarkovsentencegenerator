<?php

require_once "db_connect.php";

$id = $_REQUEST['id'] ?? -1;

if(!$mysqli){
    http_response_code(500);
    $mysqli->close();
    die(500);
}

if(!is_numeric($id) || $id <= 0){
    http_response_code(400);
    $mysqli->close();
    die(400);
}

$text_stmt = $mysqli->prepare("SELECT generated_text FROM generated WHERE id = ?");
$text_stmt->bind_param("i", $id);
$text_stmt->execute();
$text_res = $text_stmt->get_result();
if($text_res->num_rows == 0){
    http_response_code(400);
    $mysqli->close();
    die(400);
}


echo $text_res->fetch_object()->generated_text;
$mysqli->close();
?>