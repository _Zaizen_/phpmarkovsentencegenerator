<?php
function mult($v){
	return floor($v * 100);
}
/*
	@param array $weightedValues
*/
function getRandomWeightedElement(array $weightedValues) {
	$weightedValues = array_map('mult', $weightedValues);
	$rand = mt_rand(1, (int) array_sum($weightedValues));
	foreach ($weightedValues as $key => $value) {
		$rand -= $value;
		if ($rand <= 0) {
			return $key;
		}
	}
}

function selectNextWord($values) {
    return getRandomWeightedElement($values);
}

require_once "PHPMarkovChain/lib/MarkovChain.php";
require_once "db_connect.php";

$style = $_REQUEST["style"] ?? 'default';

if(!$mysqli){
    http_response_code(500);
    $mysqli->close();
    die(500);
}

$style_stmt = $mysqli->prepare("SELECT id, document FROM documents WHERE style = ?");
$style_stmt->bind_param("s", $style);
$style_stmt->execute();
$style_res = $style_stmt->get_result();
if($style_res->num_rows == 0){
    http_response_code(400);
    $mysqli->close();
    die(400);
}

$documents = [];
$document_ids = [];
while($doc = $style_res->fetch_object()){
    $documents[] = $doc->document;
    $document_ids[] = $doc->id;
}

$markov = new MarkovChain();
$markov->build(new SimpleDataSource($documents));

$keys = array_keys($markov->chain);
$result = "";
$firstWord = "";
$firstWord = $keys[array_rand($keys)];

while ($firstWord[strlen($firstWord) - 1] == ".") {
    $firstWord = $keys[array_rand($keys)];
}

$result .= $firstWord;
for ($i = 0; $i < 500; $i++) {
    if (isset($markov->chain[$firstWord]) && count($markov->chain[$firstWord]) > 0) {
        $firstWord = selectNextWord($markov->chain[$firstWord]);
        $result .= " " . $firstWord;
    } else {
        break;
    }
}
$result .= "\n";

$insert_stmt = $mysqli->prepare("INSERT INTO `generated`(generated_text) VALUES(?)");
$insert_stmt->bind_param("s", $result);
$insert_stmt->execute();
$generated_text_id = $mysqli->insert_id;

$doc_id = -1;
$insert_stmt = $mysqli->prepare("INSERT INTO generated_from_docs(generated_text_id, document_id) VALUES(?, ?)");
$insert_stmt->bind_param("ii", $generated_text_id, $doc_id);
foreach ($document_ids as $doc_id) {
    $insert_stmt->execute();
}

$permalink = "https://api.cagnocammello.com/markov/rileggi.php?id=$generated_text_id";

echo $result;
$mysqli->close();

?>
<p>Permalink</p>
<a href="<?php echo $permalink; ?>"><?php echo $permalink; ?></a>