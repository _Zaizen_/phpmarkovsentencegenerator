create table documents (
	id bigint auto_increment not null,
    style varchar(255) not null,
    document longtext not null,
    
    primary key(id)
) Engine=InnoDb;

create table generated (
	id bigint auto_increment not null,
    generated_text longtext not null,
    
    primary key(id)
)Engine=InnoDB;


create table generated_from_docs (
    generated_text_id bigint not null,
    document_id bigint not null,
    
    primary key(generated_text_id, document_id),
    foreign key(document_id) references documents(id),
    foreign key(generated_text_id) references generated(id)
)Engine=InnoDB;